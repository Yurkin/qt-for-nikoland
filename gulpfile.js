const { src, dest, watch } = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');


function css() {
  return src('sass/*.sass')
    .pipe(sass())
    .pipe(autoprefixer({
      flexbox: true

    }))
    .pipe(dest('css'))
}


exports.default = function() {
  watch('sass/*.sass', css)
};